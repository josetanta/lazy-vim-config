return {
  {
    "navarasu/onedark.nvim",
    lazy = true,
    priority = 1000,
    opts = function()
      return {
        transparent = false,
        style = "warm",
      }
    end,
  },
}
